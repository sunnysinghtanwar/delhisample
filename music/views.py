from django.http import HttpResponse
from django.template import loader

def index(request):
	template = loader.get_template('music/index.html')
	context = {
		
	}
	return HttpResponse(template.render(context,request))

# def index(request):
# 	return HttpResponse("This is a homepage of music")